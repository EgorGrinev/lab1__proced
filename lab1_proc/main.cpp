#include "stdafx.h"
#include <iostream>
#include <fstream>
#include "program.h"

using namespace std;

namespace simple_shapes
{
	void Init(struct List &lst);
	void Clear(struct List &lst);
	void In(struct List &lst, ifstream &ifst);
	void Out(struct List &lst, ofstream &ofst);
	bool Compare(Shape *first, Shape *second);
	void Sort(struct List &lst);
	void OutAphorisms(struct List &lst, ofstream &ofst);
	void MultiMethod(List &lst, ofstream &ofst);
}

using namespace simple_shapes;

int main(int argc, char* argv[])
{
	if (argc != 3)
	{
		cout << "Incorrect command line! "
		"Waited: command infile outfile" << endl;
		exit(1);
	}

	ifstream ifst(argv[1]);
	if (!ifst.is_open())
	{
		cout << "File " << argv[1] << " cannot be opened!" << endl;
		return 0;
	}
	ofstream ofst(argv[2]);

	cout << "Start" << endl;
	struct List lst;
	Init(lst);

	In(lst, ifst);
	ofst << "Filled container." << endl;
	Sort(lst);
	Out(lst, ofst);
	MultiMethod(lst, ofst);

	Clear(lst);
	ofst << "Empty container." << endl;
	Out(lst, ofst);

	cout << "Stop" << endl;
	return 0;
}

