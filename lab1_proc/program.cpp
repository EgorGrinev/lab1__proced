#include "stdafx.h"
#include "program.h"
#include <string>
#include <fstream>
#include <iostream>

using namespace std;

namespace simple_shapes
{
	struct Aphorism;
	struct Proverb;
	struct Shape;
	struct Node;
	struct List;
	struct Riddle;
	
	void Init(struct List &lst)
	{
		lst.head = NULL;
		lst.tail = NULL;
		lst.size = 0;
	}

	void Clear(struct List &lst)
	{
		while (lst.size != 0) // ���� ����������� ������ �� ������ ������� 
		{
			Node *temp = lst.head->next;
			delete lst.head; // ����������� ������ �� ��������� ��������
			lst.head = temp; // ����� ������ ������ �� ����� ���������� ��������
			lst.size--; // ���� ������� ����������. ������������ ����� ���������
		}
	}

	void In(struct List &lst, ifstream &ifst)
	{
		while (!ifst.eof())
		{
			lst.size++; // ��� ������ ���������� �������� ����������� ����� ��������� � ������
			Node  *temp = new Node; // ��������� ������ ��� ������ �������� ������
			temp->x = In(ifst); // ���������� � ���������� ������ ������ �������� x 

			if (lst.head != NULL) // � ��� ������ ���� ������ �� ������
			{
				lst.tail->next = temp; // ������ ������ � ��������� �� ��������� ��������� ����
				temp->prev = lst.tail;
				lst.tail = temp; // ��������� �������� ������� ������ ��� ���������.

			}
			else
			{
				lst.head = lst.tail = temp; // ���� ������ ���� �� ��������� ������ �������.
			}
		}
	}

	void Out(struct List &lst, ofstream &ofst)
	{
		ofst << "Container contains " << lst.size << " elements." << endl;
		Node *tempHead = lst.head; // ��������� �� ������
		int temp = lst.size; // ��������� ���������� ������ ����� ��������� � ������
		int i = 0;
		while (temp != 0) // ���� �� �������� ������� ������� �� ����� ������
		{
			ofst << i << ": ";
			i++;
			Out(*(tempHead->x), ofst); // ��������� ������� ������ �� ����� 
			tempHead = tempHead->next; // ���������, ��� ����� ��������� �������
			temp--; // ���� ������� ������, ������ �������� �� ���� ������ 
		}
	}

	void In(Aphorism &a, ifstream &ifst)
	{
		char temp[256];
		ifst.getline(temp, 256, '\n');
		ifst.getline(a.author, 256, '\n');
		
	}

	void In(Proverb &p, ifstream &ifst)
	{
		char temp[256];
		ifst.getline(temp, 256, '\n');
		ifst.getline(p.country, 256, '\n');
		
	}
	void In(Riddle &r, ifstream &ifst)
	{
		char temp[256];
		ifst.getline(temp, 256, '\n');
		ifst.getline(r.answer, 256, '\n');
		
	}
	Shape* In(ifstream &ifst)
	{
		Shape *sp;
		int k;
		ifst.clear();
		if (!(ifst >> k))
		{
			cout << "Reading Key: Incorrect data!" << endl;
			exit(1);
		}

		switch (k) {
		case 1:
			sp = new Shape;
			sp->k = Shape::key::APHORISM;
			In(sp->a, ifst);
			break;
		case 2:
			sp = new Shape;
			sp->k = Shape::key::PROVERB;
			In(sp->p, ifst);
			break;
		case 3:
			sp = new Shape;
			sp->k = Shape::key::RIDDLE;
			In(sp->r, ifst);
			break;
		default:
			cout << "Reading Key: Incorrect data!" << endl;
			exit(1);
		}
		ifst.getline(sp->text, 256, '\n');
		ifst.clear();
		
		if (!(ifst >> sp->assessment))
		{
			cout << "Reading Assessment: Incorrect data!" << endl;
			exit(1);
		}
		if (sp->assessment > 10 || sp->assessment < 0)
		{
			cout << "Reading Assessment: Incorrect data!" << endl;
			exit(1);
		}
		return sp;
	}	

	void Out(Shape &s, ofstream &ofst) {
		switch (s.k) 
		{
			case Shape::key::APHORISM:
				Out(s.a, ofst);
				break;
			case Shape::key::PROVERB:
				Out(s.p, ofst);
				break;
			case Shape::key::RIDDLE:
				Out(s.r, ofst);
				break;
			default:
				ofst << "Incorrect figure!" << endl;

		}
		ofst << ", text = " << s.text << ", assessment = " << s.assessment << ", count of punctuation marks = " << Count(s) << endl;
	}

	void Out(Aphorism &a, ofstream &ofst)
	{
		ofst << "It is Aphorism: author = " << a.author;
	}

	void Out(Proverb &p, ofstream &ofst)
	{
		ofst << "It is Proverb: country = " << p.country;
	}
	void Out(Riddle &r, ofstream &ofst)
	{
		ofst << "It is Riddle: answer = " << r.answer ;
	}
	
	int Count(Shape &s)
	{
		int result = 0;
		for (int i = 0; i < strlen(s.text); i++)
		if (s.text[i] == ',' || s.text[i] == '.' || s.text[i] == '?' ||
			s.text[i] == '!' || s.text[i] == '(' || s.text[i] == ')' ||
			s.text[i] == '"' || s.text[i] == '-' || s.text[i] == ':')
			result++;
		return result;
	}
	
	bool Compare(Shape *first, Shape *second)
	{
		int countFirst = Count(*first);
		int countSecond = Count(*second);
		return countFirst < countSecond;
	}

	void Sort(struct List &lst)
	{
		Node *p = lst.head;
		for (int i = 0; i < lst.size - 1; i++)
		{
			Node *temp = p->next;
			for (int j = i + 1; j < lst.size; j++)
			{
				if (Compare(p->x, temp->x))
				{
					Shape *tmp = p->x;
					p->x = temp->x;
					temp->x = tmp;
				}
				temp = temp->next;
			}
			p = p->next;
		}

	}
	void OutAphorisms(struct List &lst, ofstream &ofst)
	{
		ofst << "Only Aphorisms." << endl;
		Node *tempHead = lst.head; // ��������� �� ������

		int temp = lst.size; // ��������� ���������� ������ ����� ��������� � ������
		int i = 0;
		while (temp != 0) // ���� �� �������� ������� ������� �� ����� ������
		{
			
			if (tempHead->x->k == Shape::APHORISM)
			{
				ofst << i << ": ";
				i++;
				Out(*(tempHead->x), ofst); // ��������� ������� ������ �� ����� 
				
			}
			
			tempHead = tempHead->next; // ���������, ��� ����� ��������� �������
			temp--; // ���� ������� ������, ������ �������� �� ���� ������ 
		}
	}
	void MultiMethod(List &lst, ofstream &ofst)
	{
		ofst << "Multimethod." << endl;
		Node *tempHead = lst.head;
		for (int i = 0; i < lst.size - 1; i++)
		{
			Node *elem = tempHead->next;
			for (int j = i + 1; j < lst.size; j++)
			{
				switch (tempHead->x->k) {
				case Shape::APHORISM:
					switch (elem->x->k) {
					case Shape::APHORISM:
						ofst << "Aphorism and Aphorism." << endl;
						break;
					case Shape::PROVERB:
						ofst << "Aphorism and Proverb." << endl;
						break;
					case Shape::RIDDLE:
						ofst << "Aphorism and Riddle." << endl;
						break;
					default:
						ofst << "Unknown type" << endl;
					}
					break;
				case Shape::PROVERB:
					switch (elem->x->k) {
					case Shape::APHORISM:
						ofst << "Proverb and Rectangle." << endl;
						break;
					case Shape::PROVERB:
						ofst << "Proverb and Proverb." << endl;
						break;
					case Shape::RIDDLE:
						ofst << "Proverb and Riddle." << endl;
						break;
					default:
						ofst << "Unknown type" << endl;
					}
					break;
				case Shape::RIDDLE:
					switch (elem->x->k) {
					case Shape::APHORISM:
						ofst << "Riddle and Aphorism." << endl;
						break;
					case Shape::PROVERB:
						ofst << "Riddle and Proverb." << endl;
						break;
					case Shape::RIDDLE:
						ofst << "Riddle and Riddle." << endl;
						break;
					default:
						ofst << "Unknown type" << endl;
					}
					break;
				default:
					ofst << "Unknown type" << endl;
				}
				Out(*(tempHead->x), ofst);
				Out(*(elem->x), ofst);
				elem = elem->next;
			}
			tempHead = tempHead->next; //���������, ��� ����� ��������� �������
		}
	}
}